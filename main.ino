const int ABC = 27;

String table[ABC] = {
	".-",
	"-...",
	"-,-,",
	"-..",
	".",
	"..-.",
	"--.",
	"....",
	"..",
	".---",
	"-.-",
	".-..",
	"--",
	"-.",
	"---",
	".--.",
	"--.-",
	".-.",
	"...",
	"-",
	"..-",
	"...-",
	".--",
	"-..-",
	"-.--",
	"--..",
};


void setup() {
	pinMode(50, OUTPUT);
	Serial.begin(9600);
}

void loop() {
	if (Serial.available()) {
		if (emit(Serial.read())) {
			delay(500);
		}
	}
}

bool emit(char c) {
	if (c >= 'a' && c < ABC + 'a') {
		for (int i = 0; i < table[c - 'a'].length(); i++) {
			if (table[c - 'a'][i] == '.') {
				digitalWrite(50, HIGH);
				delay(100);
			} else {
				digitalWrite(50, HIGH);
				delay(300);
			}
			digitalWrite(50, LOW);
			delay(100);
		}
		return true;
	}
	return false;
}
